import Escuela from './Escuela';
import HechizoAprendido from "./HechizoAprendido";
import Clase from './Clase';

export default interface Hechizo{
    id?:number;
    nombre?:string;
    nivel?:number;
    duracion?:string;
    nombreIngles?:string;
    descripcion?:string;
    aMayorNivel?:string;
    rango?:string;
    componenteVerbal?:boolean;
    componenteSomatico?:boolean;
    componenteMaterial?:boolean;
    ritual?:boolean;
    concentracion?:boolean;
    descripcionComponente?:string;
    tiempoDeCasteo?:string;
    escuela?: Escuela;
    hechizosAprendidos?:HechizoAprendido[];
    clases?:Clase[];
    personalizacion?:boolean;
}