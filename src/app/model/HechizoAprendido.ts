import Personaje from './Personaje';
import Hechizo from './Hechizo';

export default interface HechizoAprendido{

    estaAprendido?:boolean
    personaje?:Personaje
    hechizo?:Hechizo;

}