import Clase from "./Clase";
import Raza from './Raza';
import HechizoAprendido from './HechizoAprendido';

export default interface Personaje{
    id?:number
    nombre?:string
    clase?:Clase
    raza?:Raza
    hechizosAprendidos?: HechizoAprendido[];

}