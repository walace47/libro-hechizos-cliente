import Personaje from "./Personaje"

export default interface Clase{
    id?:number
    nombre?:string
    personajes?:Personaje[]

}