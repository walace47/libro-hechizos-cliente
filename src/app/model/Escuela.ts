import Hechizo from './Hechizo';

export default interface Escuela{
    id?:number
    nombre?:string
    hechizos?: Hechizo[];
}