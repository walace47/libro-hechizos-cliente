import Personaje from "./Personaje"

export default class Escuela{
    id?:number
    nombre?:string
    personajes?:Personaje[]
}