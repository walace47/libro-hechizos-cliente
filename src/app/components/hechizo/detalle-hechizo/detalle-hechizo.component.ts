import { Component, OnInit, Input } from '@angular/core';
import Hechizo from 'src/app/model/Hechizo';

@Component({
  selector: 'app-detalle-hechizo',
  templateUrl: './detalle-hechizo.component.html',
  styles: [`
  `]
})
export class DetalleHechizoComponent implements OnInit {
  @Input('hechizo') hechizo: Hechizo;

  constructor() { 
  }

  ngOnInit() {
    console.log(this.hechizo)

  }

}
