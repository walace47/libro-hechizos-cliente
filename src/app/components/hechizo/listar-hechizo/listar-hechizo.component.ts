import {
  Component,
  OnInit,
  ViewChild,
} from "@angular/core";
import Hechizo from "src/app/model/Hechizo";
import { HechizoService, ClaseService } from "src/app/services";
import { SelectItem } from "primeng/api";
import Clase from 'src/app/model/Clase';
import { DataView } from 'primeng/dataview/dataview';
import { intersectionBy } from 'lodash'


@Component({
  selector: "app-listar-hechizo",
  templateUrl: "./listar-hechizo.component.html",
  styleUrls: ["./listar-hechizo.component.css"]
})
export class ListarHechizoComponent implements OnInit {
  @ViewChild('dv',{static:false}) tableRef:DataView ;
  public hechizos: Hechizo[];
  public selectedHechizo: Hechizo;
  public displayDialog: boolean;
  public sortOptions: SelectItem[];
  public sortKey: string;
  public sortField: string;
  public sortOrder: number;
  public expanse:boolean = true;
  public descripcionVista:any = {};
  public crearHechizoDialog:boolean = false
  public clases:Clase[];
  public filterClases:Clase[] = [];
  public filtroTexto: string = "";


  constructor(public service: HechizoService,private _claseServices:ClaseService) {
}

  ngOnInit() {
     this._claseServices.get().subscribe(clases => this.clases = clases);

    this.service
      .getAll(
        JSON.stringify(["id", "nombre", "nivel","descripcion","aMayorNivel","rango","duracion","tiempoDeCasteo"]),
        JSON.stringify(["clases","escuela"])
      )
      .subscribe(hechizos => {
        this.hechizos = hechizos;
        this.hechizos.forEach(hechizo => this.descripcionVista[hechizo.id] = {hechizo,estadoDescripcion:false})
      });

      this.sortOptions = [
        {label:'Nombre',value:'nombre'},
        {label:'Menor Nivel',value:'nivel'},
        {label:'Mayor Nivel',value:'!nivel'}]


  }
  estaAbierto(hechizo:Hechizo):boolean{
    return this.descripcionVista[hechizo.id].estadoDescripcion
  }

  agregarHechizo(event: Event) {
    this.crearHechizoDialog = true;

    this.displayDialog = true;
    event.preventDefault();
  }

  verDescripcionVista(event: Event, hechizo: Hechizo){
    this.selectedHechizo = hechizo;
    let nuevoEstado = !this.descripcionVista[hechizo.id].estadoDescripcion;
    this.descripcionVista[hechizo.id].estadoDescripcion = nuevoEstado;
    event.preventDefault();
  }

  filtrarClase(){
    if(this.filterClases.length == 0){
      this.tableRef.filteredValue= this.tableRef.value;
    }else{
      this.tableRef.filteredValue = this.tableRef.value.filter(element =>  intersectionBy(element.clases,this.filterClases,'id').length > 0);
    }
    if (this.tableRef.paginator) {
      this.tableRef.first = 0;
      this.tableRef.totalRecords = this.tableRef.filteredValue ? this.tableRef.filteredValue.length : this.tableRef.value ? this.tableRef.value.length : 0;
  }
  this.tableRef.filter(this.filtroTexto)

  }

  onSortChange(event) {
    let value = event.value;
    console.log(value)
    if (value.indexOf("!") === 0) {
      this.sortOrder = -1;
      this.sortField = value.substring(1, value.length);
    } else {
      this.sortOrder = 1;
      this.sortField = value;
    }
  }

  onDialogHide() {
    this.crearHechizoDialog = false;
    this.selectedHechizo = null;
  }

  ngOnDestroy(): void {}
}
