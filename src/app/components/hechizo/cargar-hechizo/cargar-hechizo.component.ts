import { Component, OnInit } from '@angular/core';
import Hechizo from 'src/app/model/Hechizo';
import { ClaseService, EscuelaService, HechizoService } from 'src/app/services';
import Clase from 'src/app/model/Clase';
import Escuela from 'src/app/model/Raza';
import { Observable } from 'rxjs';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-cargar-hechizo',
  templateUrl: './cargar-hechizo.component.html',
  styleUrls: ['./cargar-hechizo.component.css']
})
export class CargarHechizoComponent implements OnInit {
  public hechizo:Hechizo;
  public $escuela:Observable<Escuela[]>;
  public $clases:Observable<Clase[]>;

  constructor(private messageService: MessageService,private _claseServices:ClaseService,private _escuelaService:EscuelaService, private _hechizoService:HechizoService) { 
    
    this.hechizo = {
      descripcion:"",
      nombre:"",
      nombreIngles:"",
      rango:"",
      nivel:0,
      componenteMaterial:false,
      componenteSomatico:false,
      componenteVerbal:false,
      descripcionComponente:"",
      clases:[],
      concentracion:false,
      ritual:false,
      escuela:{},
      aMayorNivel: "",
      duracion:"",
      tiempoDeCasteo:"",
      personalizacion:true
    }
  }

  ngOnInit() {

    this.$escuela = this._escuelaService.getAll();
    this.$clases = this._claseServices.get();
  }

  crear(){
    console.log(this.hechizo.escuela)
    if(this.hechizo.escuela || this.hechizo.clases.length == 0){
      if(this.hechizo.escuela){
        this.messageService.add({severity:'error', summary: 'Validation error', detail:'Por favor agregar alguna escuela'});
      }
      if(this.hechizo.clases.length == 0){
        this.messageService.add({severity:'error', summary: 'Validation error', detail:'Por favor agregar alguna clase'});
      }
    }else{
      this._hechizoService.create(this.hechizo).subscribe(res => console.log(res));
    }
  }

  onConfirm() {
    this.messageService.clear('c');
}

onReject() {
    this.messageService.clear('c');
}

clear() {
    this.messageService.clear();
}

}
