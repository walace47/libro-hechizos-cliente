import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CargarHechizoComponent } from './components/hechizo/cargar-hechizo/cargar-hechizo.component';
import { ListarHechizoComponent } from './components/hechizo/listar-hechizo/listar-hechizo.component';
import { DataTablesModule } from 'angular-datatables';


const routes: Routes = [
  {path:'hechizo/:id',component:CargarHechizoComponent},
  {path:'hechizos',component:ListarHechizoComponent},

  ];
  

@NgModule({
  declarations:[],
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
