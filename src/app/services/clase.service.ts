import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import Clase from '../model/Clase';

@Injectable({
  providedIn: 'root'
})
export class ClaseService {

  constructor(private http:HttpClient) { }

  get(){
    return this.http.get<Clase[]>("http://localhost:3000/clase/todos")
  }
}
