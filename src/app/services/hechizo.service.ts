import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import Hechizo from '../model/Hechizo';



@Injectable({
  providedIn: 'root'
})
export class HechizoService {

  public hechizos:Hechizo[];

  constructor(private http:HttpClient) { 
  }


  create(hechizo:Hechizo){
    return this.http.post("http://localhost:3000/hechizo",hechizo)
  }

  getAll(select?:string,relations?:string){
    return this.http.get<Hechizo[]>(`http://localhost:3000/hechizo/todos?select=${select}&relations=${relations}`);
  }


}
