import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import Escuela from '../model/Raza';

@Injectable({
  providedIn: 'root'
})
export class EscuelaService {

  constructor(private http:HttpClient) { 
  }

  getAll(){
    return this.http.get<Escuela[]>("http://localhost:3000/escuela/todos")
  }
}
