import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { HttpClientModule} from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { NgSelectModule } from '@ng-select/ng-select';

import { DataTablesModule } from 'angular-datatables';

import { AngularEditorModule } from '@kolkov/angular-editor';
import { CargarHechizoComponent } from './components/hechizo/cargar-hechizo/cargar-hechizo.component';

import {ClaseService,EscuelaService,HechizoService} from "./services";
import { NavbarComponent } from './components/shared/navbar/navbar.component';
import { ListarHechizoComponent } from './components/hechizo/listar-hechizo/listar-hechizo.component';

import {DataViewDemoModule} from './modules/primeng/primeng.module'
import {DataViewModule} from 'primeng/dataview';

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { DetalleHechizoComponent } from './components/hechizo/detalle-hechizo/detalle-hechizo.component';
import { MessageService } from 'primeng/api';



@NgModule({
  declarations: [
    AppComponent,
    CargarHechizoComponent,
    NavbarComponent,
    ListarHechizoComponent,
    DetalleHechizoComponent
  ],
  imports: [
    BrowserAnimationsModule,
    DataViewDemoModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    AngularEditorModule,
    FormsModule,
    DataTablesModule,
    NgSelectModule,
    DataViewModule
  ],
  providers: [
    MessageService,
    ClaseService,
    EscuelaService,
    HechizoService],
  bootstrap: [AppComponent]
})
export class AppModule { }
