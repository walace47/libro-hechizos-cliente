import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule} from '@angular/forms';
import {DataViewModule} from 'primeng/dataview';
import {PanelModule} from 'primeng/panel';
import {InputTextModule} from 'primeng/inputtext';
import {ButtonModule} from 'primeng/button';
import {DialogModule} from 'primeng/dialog';
import {DropdownModule} from 'primeng/dropdown';
import {TabViewModule} from 'primeng/tabview';
import {ToastModule} from 'primeng/toast';


import {CodeHighlighterModule} from 'primeng/codehighlighter';
import {AccordionModule} from 'primeng/accordion';
import {TreeModule} from 'primeng/tree';
import {ChipsModule} from 'primeng/chips';
import {MultiSelectModule} from 'primeng/multiselect';


@NgModule({
	imports: [
    ToastModule,
    MultiSelectModule,
    ChipsModule,
    TreeModule,
    AccordionModule,
    CommonModule,
    FormsModule,
    DataViewModule,
    PanelModule,
    DialogModule,
    DropdownModule,
    TabViewModule,
    InputTextModule,
    ButtonModule,
    CodeHighlighterModule
  ],
  exports: [
    ToastModule,
    MultiSelectModule,
    ChipsModule,
    TreeModule,
    AccordionModule,
    CommonModule,
    FormsModule,
    DataViewModule,
    PanelModule,
    DialogModule,
    DropdownModule,
    TabViewModule,
    InputTextModule,
    ButtonModule,
    CodeHighlighterModule
],
	declarations: [
	]
})
export class DataViewDemoModule {}

